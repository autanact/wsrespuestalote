#Bitacora de cambios servicio WSRespuestaLote
#Freddy Molina
#Fecha creación:28/11/2015
#Descripión general: Servicio WSRespuestaLote para enviar direcciones Excepcionadas a los lotes
#Últimas modificaciónes
	28/11/2015 FMS Se crea WSDL e implementación
	03/03/2016 FMS Documentación
#Versión actual
	22/02/2016 v2.0