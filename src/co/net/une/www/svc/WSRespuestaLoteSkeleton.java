
/**
 * WSRespuestaLoteSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.3  Built on : Aug 10, 2007 (04:45:47 LKT)
 * with extensions for GE Smallworld GeoSpatial Server
 */
    package co.net.une.www.svc;
    import java.util.Map;
    import java.util.HashMap;    
    
    import org.apache.axis2.engine.AxisError;
    
    import com.gesmallworld.gss.lib.exception.GSSException;
    import com.gesmallworld.gss.webservice.WebServiceRequest;
    /**
     *  WSRespuestaLoteSkeleton java skeleton for the axisService
     */
    public class WSRespuestaLoteSkeleton extends WebServiceRequest
        {
        
	
	private static final String serviceName = "ejb/WSRespuestaLoteLocal";
	
     
         
        /**
         * Auto generated method signature
         
         
                                     * @param nombreArchivo
                                     * @param fechaEnvioLote
                                     * @param estadoLote
                                     * @param comentarioEstado
         */
        

                 public co.net.une.www.svc.WSRespuestaLoteRSType respuestaLote
                  (
                  co.net.une.www.svc.BoundedString100 nombreArchivo,co.net.une.www.svc.BoundedString19 fechaEnvioLote,co.net.une.www.svc.BoundedString2 estadoLote,co.net.une.www.svc.BoundedString400 comentarioEstado
                  )
            {
                //GSS generated code
		Map<String,Object> params = new HashMap<String,Object>();
                  params.put("nombreArchivo",nombreArchivo);params.put("fechaEnvioLote",fechaEnvioLote);params.put("estadoLote",estadoLote);params.put("comentarioEstado",comentarioEstado);
		try{
		
			return (co.net.une.www.svc.WSRespuestaLoteRSType)
			this.makeStructuredRequest(serviceName, "respuestaLote", params);
		}catch(GSSException e){
                    // Modify if specific faults are required
                    throw new AxisError(e.getLocalizedMessage()+": "+e.getRootThrowable().getLocalizedMessage(), e.getRootThrowable());
                }
        }
     
    }
    