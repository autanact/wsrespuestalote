
/**
 * WSRespuestaLoteMessageReceiverInOut.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.4.1  Built on : Aug 13, 2008 (05:03:35 LKT)
 */
        package co.net.une.www.svc;

        /**
        *  WSRespuestaLoteMessageReceiverInOut message receiver
        */

        public class WSRespuestaLoteMessageReceiverInOut extends org.apache.axis2.receivers.AbstractInOutMessageReceiver{


        public void invokeBusinessLogic(org.apache.axis2.context.MessageContext msgContext, org.apache.axis2.context.MessageContext newMsgContext)
        throws org.apache.axis2.AxisFault{

        try {

        // get the implementation class for the Web Service
        Object obj = getTheImplementationObject(msgContext);

        WSRespuestaLoteSkeleton skel = (WSRespuestaLoteSkeleton)obj;
        //Out Envelop
        org.apache.axiom.soap.SOAPEnvelope envelope = null;
        //Find the axisOperation that has been set by the Dispatch phase.
        org.apache.axis2.description.AxisOperation op = msgContext.getOperationContext().getAxisOperation();
        if (op == null) {
        throw new org.apache.axis2.AxisFault("Operation is not located, if this is doclit style the SOAP-ACTION should specified via the SOAP Action to use the RawXMLProvider");
        }

        java.lang.String methodName;
        if((op.getName() != null) && ((methodName = org.apache.axis2.util.JavaUtils.xmlNameToJava(op.getName().getLocalPart())) != null)){

        

            if("respuestaLote".equals(methodName)){
                
                co.net.une.www.svc.WSRespuestaLoteRS wSRespuestaLoteRS5 = null;
	                        co.net.une.www.svc.WSRespuestaLoteRQ wrappedParam =
                                                             (co.net.une.www.svc.WSRespuestaLoteRQ)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    co.net.une.www.svc.WSRespuestaLoteRQ.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               wSRespuestaLoteRS5 =
                                                   
                                                   
                                                           wraprespuestaLote(
                                                       
                                                        

                                                        
                                                       skel.respuestaLote(
                                                            
                                                                getNombreArchivo(wrappedParam)
                                                            ,
                                                                getFechaEnvioLote(wrappedParam)
                                                            ,
                                                                getEstadoLote(wrappedParam)
                                                            ,
                                                                getComentarioEstado(wrappedParam)
                                                            )
                                                    
                                                         )
                                                     ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), wSRespuestaLoteRS5, false);
                                    
            } else {
              throw new java.lang.RuntimeException("method not found");
            }
        

        newMsgContext.setEnvelope(envelope);
        }
        }
        catch (java.lang.Exception e) {
        throw org.apache.axis2.AxisFault.makeFault(e);
        }
        }
        
        //
            private  org.apache.axiom.om.OMElement  toOM(co.net.une.www.svc.WSRespuestaLoteRQ param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(co.net.une.www.svc.WSRespuestaLoteRQ.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(co.net.une.www.svc.WSRespuestaLoteRS param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(co.net.une.www.svc.WSRespuestaLoteRS.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, co.net.une.www.svc.WSRespuestaLoteRS param, boolean optimizeContent)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(co.net.une.www.svc.WSRespuestaLoteRS.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    

                        private co.net.une.www.svc.BoundedString100 getNombreArchivo(
                        co.net.une.www.svc.WSRespuestaLoteRQ wrappedType){
                        
                                return wrappedType.getWSRespuestaLoteRQ().getNombreArchivo();
                            
                        }
                     

                        private co.net.une.www.svc.BoundedString19 getFechaEnvioLote(
                        co.net.une.www.svc.WSRespuestaLoteRQ wrappedType){
                        
                                return wrappedType.getWSRespuestaLoteRQ().getFechaEnvioLote();
                            
                        }
                     

                        private co.net.une.www.svc.BoundedString2 getEstadoLote(
                        co.net.une.www.svc.WSRespuestaLoteRQ wrappedType){
                        
                                return wrappedType.getWSRespuestaLoteRQ().getEstadoLote();
                            
                        }
                     

                        private co.net.une.www.svc.BoundedString400 getComentarioEstado(
                        co.net.une.www.svc.WSRespuestaLoteRQ wrappedType){
                        
                                return wrappedType.getWSRespuestaLoteRQ().getComentarioEstado();
                            
                        }
                     
                        private co.net.une.www.svc.WSRespuestaLoteRQType getrespuestaLote(
                        co.net.une.www.svc.WSRespuestaLoteRQ wrappedType){
                            return wrappedType.getWSRespuestaLoteRQ();
                        }
                        
                        
                    
                         private co.net.une.www.svc.WSRespuestaLoteRS wraprespuestaLote(
                            co.net.une.www.svc.WSRespuestaLoteRSType innerType){
                                co.net.une.www.svc.WSRespuestaLoteRS wrappedElement = new co.net.une.www.svc.WSRespuestaLoteRS();
                                wrappedElement.setWSRespuestaLoteRS(innerType);
                                return wrappedElement;
                         }
                    


        /**
        *  get the default envelope
        */
        private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory){
        return factory.getDefaultEnvelope();
        }


        private  java.lang.Object fromOM(
        org.apache.axiom.om.OMElement param,
        java.lang.Class type,
        java.util.Map extraNamespaces) throws org.apache.axis2.AxisFault{

        try {
        
                if (co.net.une.www.svc.WSRespuestaLoteRQ.class.equals(type)){
                
                           return co.net.une.www.svc.WSRespuestaLoteRQ.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (co.net.une.www.svc.WSRespuestaLoteRS.class.equals(type)){
                
                           return co.net.une.www.svc.WSRespuestaLoteRS.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
        } catch (java.lang.Exception e) {
        throw org.apache.axis2.AxisFault.makeFault(e);
        }
           return null;
        }



    

        /**
        *  A utility method that copies the namepaces from the SOAPEnvelope
        */
        private java.util.Map getEnvelopeNamespaces(org.apache.axiom.soap.SOAPEnvelope env){
        java.util.Map returnMap = new java.util.HashMap();
        java.util.Iterator namespaceIterator = env.getAllDeclaredNamespaces();
        while (namespaceIterator.hasNext()) {
        org.apache.axiom.om.OMNamespace ns = (org.apache.axiom.om.OMNamespace) namespaceIterator.next();
        returnMap.put(ns.getPrefix(),ns.getNamespaceURI());
        }
        return returnMap;
        }

        private org.apache.axis2.AxisFault createAxisFault(java.lang.Exception e) {
        org.apache.axis2.AxisFault f;
        Throwable cause = e.getCause();
        if (cause != null) {
            f = new org.apache.axis2.AxisFault(e.getMessage(), cause);
        } else {
            f = new org.apache.axis2.AxisFault(e.getMessage());
        }

        return f;
    }

        }//end of class
    